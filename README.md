# Hur Website

Hur is for freelancers who are actually free to save and share their own data and set their own rates.

## How it works

The idea is to collect user created profiles in a website and make it available for search and hiring in simple format.

It's based on data from the [Hur project](https://gitlab.com/uak/hur/)

## Screenshot

![searching for keywords](https://gitlab.com/uak/hur_website/-/raw/main/images/search_in_hur_website.gif)

## Technical Details

This project is using:

* Pelican ([Flex theme](https://github.com/alexandrevicenzi/Flex))
* Pelcian-search ([stork](https://stork-search.net/))
* Snake-md
* toml

## License

AGPL-V3
